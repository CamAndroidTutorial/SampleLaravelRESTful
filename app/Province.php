<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "provinces";
    public $primaryKey = "PROCODE";
    public $timestamps = false;

    public function districts(){
        return $this->hasMany('App\District', 'PCode', 'PROCODE');
    }
}
