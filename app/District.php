<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = "districts";
    public $primaryKey = "DCode";
    public $timestamps = false;

    public function province(){
        return $this->belongsTo('App\Province', 'PCode', 'PROCODE');
    }


}
